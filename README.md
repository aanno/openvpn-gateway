Brückenkopf in Eigenregie
=========================

Selbst gebautes OpenVPN-Gateway in der Amazon-Cloud
---------------------------------------------------

c't 08/2013, S. 130, mid

Setup-Skript für Debian-AMIs
OpenVPN-Konfigurationsdateien für Linux, Windows, iOS und Android

Original Link: http://www.ct.de/1308130

--
Modified by Thomas Pasch

#### CHANGES
 - Using noip (http://www.noip.com) instead of dyndns
 - Using noip update binary
 - Host space is *.no-ip.biz (free)
 - Switch to certificate created with easy-rsa2 (http://wiki.ubuntuusers.de/OpenVPN). 
 - Hence we use (3) PEM files instead of one PKCS#12/X.509 again.
 - Rewritten the complete OpenVPN server.conf, based on OpenVPN documentation:
    * Now using UDP instead of TCP for better performance. 
    * Pushing opendns.com nameservers. 
    * Restricting the number of connection per certificate to one. 
    * Enabled certificate blacklisting. 
    * And much more...
 - Added Dante SOCKS5 (http://www.inet.no/dante/) that can be used over OpenVPN. This way default net traffic does not run through VPN in an alternative configuration.

#### VERSIONS
 - noip update script 2.1.9-1
 - dante socks5 1.3.2
