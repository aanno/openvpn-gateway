#!/bin/bash

if [ "$(id -u)" -ne 0 ]; then
  echo "Dieses Skript benötigt Root-Rechte."
  exit 1
fi

echo "Paketquellen aktualisieren"
apt-get update || exit 1
echo "Updates einspielen"
apt-get -y --force-yes upgrade || exit 1

echo "Zusätzliche Software einspielen"
apt-get -y install openvpn iptables-persistent dnsmasq lynx bind9-host || exit 1

# Beginn des Tars ermitteln
FirstLine=$(grep -anso -m 1 '^### Begin Binary ###' $0)
FirstLine=${FirstLine%%:*}
FirstLine=$((${FirstLine}+1))
if [ "${FirstLine}" = "1" ]; then
  echo "Binärteil nicht gefunden."
  exit 1
fi

# Tar auspacken
echo "Konfigurationsdateien auspacken"
tail -n +${FirstLine} $0 | tar x -C / || exit 1

echo "Dynamischen Hostnamen aktualisieren"
/etc/network/if-up.d/dyndns || exit 1

echo "NAT-Regel einrichten und speichern"
iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE || exit 1
iptables-save > /etc/iptables/rules || exit 1

echo "IPv4- und IPv6-Forwarding aktivieren"
sed -i -e 's/^#net\.ipv4\.ip_forward=.*/net.ipv4.ip_forward=1/' /etc/sysctl.conf
# sed -i -e 's/^#net\.ipv6\.conf\.all\.forwarding=.*/net.ipv6.conf.all.forwarding=1/' /etc/sysctl.conf
sysctl -p || exit 1

echo "OpenVPN starten"
touch /usr/share/openssl-blacklist/blacklist.RSA-4096 || exit 1
/etc/init.d/openvpn stop
/etc/init.d/openvpn start || exit 1

echo "DNS-Weiterleitung starten"
/etc/init.d/dnsmasq stop
/etc/init.d/dnsmasq start || exit 1

echo "Fertig."
exit 0

### Begin Binary ### Do NOT change this Label!
